package com.javier.ws.rest.aplicacion;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.javier.ws.rest.config.configuracion;
import com.javier.ws.rest.exception.CustomException;
import com.javier.ws.rest.vo.VOJugada;

public class Jugada {
	public static void main(String[] args) {
		
	configuracion config = new configuracion();
	int numeroFinal= config.getNumFinal();
	
	Random random = new Random();
	int numeroInicial = random.nextInt(20) + 1;
	
	VOJugada Ronda = new VOJugada();
	Ronda.setNumInicial(numeroInicial);
	Ronda.setNumFinal(numeroFinal);
	
	List<String> cadena = crearJugada(Ronda);
	System.out.println(cadena);
	escritura(cadena);

	}
	
	public static List<String> crearJugada(VOJugada vo) {
		List<String> cadena = new ArrayList<String>();
		Date date = new Date();
		try{
			if (vo.getNumInicial()<vo.getNumFinal()) {
				for (int i = vo.getNumInicial(); i <= vo.getNumFinal(); i++) {
					cadena.add(fizzbuzz(i));
				}
				cadena.add(date.toString());
			}else{
				throw new CustomException(111);
			}
		}catch(Exception e){
			System.out.println(e);
		}
		return cadena;
		}
	
	public static String fizzbuzz(int numLista) {
		try {
			if ((numLista % 3 == 0) && (numLista % 5 == 0)) {
				return "fizzbuzz";
			}

			if (numLista % 5 == 0) {
				return "buzz";
			}

			if (numLista % 3 == 0) {
				return "fizz";
			}
			return Integer.toString(numLista);
			
		}catch(Exception e) {
			System.out.println(e);
			return null;
		}
	}
	
	public static void escritura(List<String> list) {
			BufferedWriter bw = null;
		    FileWriter fw = null;
		    try {
		    	File miDir = new File (".");
		        File file = new File(miDir.getCanonicalPath()+ "/Rondas.txt");
		        if (!file.exists()) {
		            file.createNewFile();
		        }
		        fw = new FileWriter(file.getAbsoluteFile(), true);
		        bw = new BufferedWriter(fw);
		        for (int i = 0; i < list.size(); i++) {
	                bw.write(list.get(i)+ ", ");
	            }
		        bw.write("\r\n");
	            bw.write("\r\n");
		    } catch (Exception e) {
		        e.printStackTrace();
		    } finally {
		        try {
		            if (bw != null)
		                bw.close();
		            if (fw != null)
		                fw.close();
		        } catch (Exception ex) {
		            ex.printStackTrace();
		        }
		    }
	}

}
