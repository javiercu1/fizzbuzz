package com.javier.ws.rest.vo;

import java.util.Date;

public class VOJugada {
	private int numInicial;
	private int numFinal;
	private Date fecha;
	
	public int getNumInicial() {
		return numInicial;
	}
	public void setNumInicial(int numInicial) {
		this.numInicial = numInicial;
	}
	public int getNumFinal() {
		return numFinal;
	}
	public void setNumFinal(int numFinal) {
		this.numFinal = numFinal;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

}
