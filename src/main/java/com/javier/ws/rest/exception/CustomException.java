package com.javier.ws.rest.exception;

public class CustomException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int intError;
    
    public CustomException(int codigoError){
        super();
        this.intError=codigoError;
    }
     
    @Override
    public String getMessage(){
         
        String mensaje="";
         
        switch(intError){
            case 111:
                mensaje="Error, el número random inicial es mayor que el final escogido de la configuración";
                break;
        }
         
        return mensaje;
         
    }
}
