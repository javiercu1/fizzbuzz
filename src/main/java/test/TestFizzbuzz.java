package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import com.javier.ws.rest.aplicacion.Jugada;
import com.javier.ws.rest.vo.VOJugada;

public class TestFizzbuzz {

	@Test
	public void testCrearJugada() {
		VOJugada Ronda = new VOJugada();
		Ronda.setNumInicial(1);
		Ronda.setNumFinal(3);
		Date date = new Date();
		
		List<String> resultado = Jugada.crearJugada(Ronda);
		List<String> esperado = new ArrayList<String>();
		esperado.add("1");
		esperado.add("2");
		esperado.add("fizz");
		esperado.add(date.toString());
		assertEquals(esperado, resultado);
		
		
		}
	
	@Test
	public void testFizzbuzz() {
		String resultado = Jugada.fizzbuzz(15);
		String esperado = "fizzbuzz";
		assertEquals(resultado, esperado);
		
	}
	
//	@Test
//	public void escritura() {
//			assert
//	}
//  No sé hacer el test unitario de la escritura de una archivo.
	

}
